﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using log4net;

namespace AwCodingChallengeApi.Controllers
{
    public abstract class BaseApiController : ApiController
    {
        protected readonly ILog Log;
        protected BaseApiController()
        {
            Log = LogManager.GetLogger(GetType());
        }

        public override async Task<HttpResponseMessage> ExecuteAsync(HttpControllerContext controllerContext, CancellationToken cancellationToken)
        {
            // tracking and logging logic here.
            if (Log.IsInfoEnabled)
            {
                Log.Info($"HTTP {controllerContext.Request.Method.Method} request for {controllerContext.Controller.GetType().Name}.");
            }
            return await base.ExecuteAsync(controllerContext, cancellationToken);
        }
    }
}
