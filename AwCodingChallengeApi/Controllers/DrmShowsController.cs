﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using AwCodingChallengeApi.Models;

namespace AwCodingChallengeApi.Controllers
{
    public class DrmShowsController : BaseApiController
    {
        [ResponseType(typeof(ResponseModel))]
        public HttpResponseMessage Post([FromBody]RequestModel request)
        {
            try
            {
                if (request == null)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new BadRequestModel("Could not decode request: JSON parsing failed"));
                }
                var responseModel = new ResponseModel
                {
                    Response = request.Payload.Where(p => p.Drm && p.EpisodeCount > 0).Select(p => new ShowModel
                    {
                        Image = p.Image?.ShowImage,
                        Slug = p.Slug,
                        Title = p.Title
                    }).ToList()
                };
                return Request.CreateResponse(HttpStatusCode.OK, responseModel);
            }
            catch (Exception ex)
            {
                if (Log.IsErrorEnabled)
                {
                    Log.Error("Unexpected error!", ex);
                }
                return Request.CreateResponse(HttpStatusCode.OK, new BadRequestModel(ex.Message));
            }
        }
    }
}
