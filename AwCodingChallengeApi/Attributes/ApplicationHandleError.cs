﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using log4net;

namespace AwCodingChallengeApi.Attributes
{
    public class ApplicationHandleError : HandleErrorAttribute
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ApplicationHandleError));
        public override void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled && filterContext.Exception != null)
            {
                if (Log.IsErrorEnabled) Log.Error("Unhandled Application Exception", filterContext.Exception);
                filterContext.Result = new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
            }
            base.OnException(filterContext);
        }
    }
}