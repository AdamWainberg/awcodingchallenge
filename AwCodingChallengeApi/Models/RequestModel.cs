﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AwCodingChallengeApi.Models
{
    public class RequestModel
    {
        public List<ShowDetailModel> Payload { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
        public int TotalRecords { get; set; }
    }
}