﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AwCodingChallengeApi.Models
{
    public class BadRequestModel
    {
        public BadRequestModel(string error)
        {
            Error = error;
        }

        public string Error { get; set; }
    }
}