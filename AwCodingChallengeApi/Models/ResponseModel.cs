﻿using System.Collections.Generic;

namespace AwCodingChallengeApi.Models
{
    public class ResponseModel
    {
        public List<ShowModel> Response { get; set; }
    }
}