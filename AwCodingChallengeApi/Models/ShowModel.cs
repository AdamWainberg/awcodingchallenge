﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AwCodingChallengeApi.Models
{
    public class ShowModel
    {
        public string Image { get; set; }
        public string Slug { get; set; }
        public string Title { get; set; }
    }
}