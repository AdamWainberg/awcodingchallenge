﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AwCodingChallengeApi.Models
{
    public class EpisodeModel
    {
        public object Channel { get; set; }
        public string ChannelLogo { get; set; }
        public string Date { get; set; }
        public string Html { get; set; }
        public string Url { get; set; }
    }
}