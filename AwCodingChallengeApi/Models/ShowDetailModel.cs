﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AwCodingChallengeApi.Models
{
    public class ShowDetailModel
    {
        public string Slug { get; set; }
        public string Title { get; set; }
        public string TvChannel { get; set; }

        public string Country { get; set; }
        public string Description { get; set; }
        public bool Drm { get; set; }
        public int EpisodeCount { get; set; }
        public string Genre { get; set; }
        public ImageModel Image { get; set; }
        public string Language { get; set; }
        public EpisodeModel NextEpisode { get; set; }
        public string PrimaryColour { get; set; }
        public List<SeasonModel> Seasons { get; set; }
    }
}