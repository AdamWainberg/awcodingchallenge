﻿using System.IO;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AwCodingChallengeApi.Controllers;
using AwCodingChallengeApi.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace AwCodingChallengeApi.Tests.Controllers
{
    [TestClass]
    public class DrmShowsControllerTest
    {

        [TestMethod]
        public void PostValidStatusCode()
        {
            // Arrange
            var controller = new DrmShowsController
            {
                Request = new HttpRequestMessage(),
                Configuration = new HttpConfiguration()
            };

            var jsonRequest = File.ReadAllText(@"SampleFiles\sample_request.json");
            var request = JsonConvert.DeserializeObject<RequestModel>(jsonRequest);

            // Act
            var response = controller.Post(request);
            
            // Assert
            Assert.AreEqual(response.StatusCode, HttpStatusCode.OK);

        }


        [TestMethod]
        public void PostValidResponse()
        {
            // Arrange
            var controller = new DrmShowsController
            {
                Request = new HttpRequestMessage(),
                Configuration = new HttpConfiguration()
            };

            var jsonRequest = File.ReadAllText(@"SampleFiles\sample_request.json");
            var request = JsonConvert.DeserializeObject<RequestModel>(jsonRequest);

            // Act
            var response = controller.Post(request);

            // Assert
            ResponseModel actualResponse;
            Assert.IsTrue(response.TryGetContentValue(out actualResponse));

        }

        [TestMethod]
        public void PostValidResponseData()
        {
            // Arrange
            var controller = new DrmShowsController
            {
                Request = new HttpRequestMessage(),
                Configuration = new HttpConfiguration()
            };

            var jsonRequest = File.ReadAllText(@"SampleFiles\sample_request.json");
            var jsonResponse = File.ReadAllText(@"SampleFiles\sample_response.json");
            var request = JsonConvert.DeserializeObject<RequestModel>(jsonRequest);

            // Act
            var response = controller.Post(request);
            
            // Assert
            ResponseModel actualResponse;
            if (response.TryGetContentValue(out actualResponse))
            {
                var expectedResponse = JsonConvert.DeserializeObject<ResponseModel>(jsonResponse);
                var expected = JsonConvert.SerializeObject(expectedResponse);
                var actual = JsonConvert.SerializeObject(actualResponse);
                Assert.AreEqual(expected, actual);
            }
        }


        [TestMethod]
        public void PostInvalidStatusCode()
        {
            // Arrange
            var controller = new DrmShowsController
            {
                Request = new HttpRequestMessage(),
                Configuration = new HttpConfiguration()
            };

            // Act
            var response = controller.Post(null);

            // Assert
            Assert.AreEqual(response.StatusCode, HttpStatusCode.BadRequest);
            
        }

        [TestMethod]
        public void PostInvalidResponse()
        {
            // Arrange
            var controller = new DrmShowsController
            {
                Request = new HttpRequestMessage(),
                Configuration = new HttpConfiguration()
            };
            
            // Act
            var response = controller.Post(null);

            // Assert
            BadRequestModel actualResponse;
            Assert.IsTrue(response.TryGetContentValue(out actualResponse));
        }

        [TestMethod]
        public void PostInvalidResponseData()
        {
            // Arrange
            var controller = new DrmShowsController
            {
                Request = new HttpRequestMessage(),
                Configuration = new HttpConfiguration()
            };

            // Act
            var response = controller.Post(null);

            // Assert
            BadRequestModel actualResponse;
            if (response.TryGetContentValue(out actualResponse))
            {
                Assert.AreEqual("Could not decode request: JSON parsing failed", actualResponse.Error);
            }
        }
    }
}
